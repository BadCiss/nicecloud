// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import axios from 'axios'
import VueAxios from 'vue-axios'
//import EventBus from './../event-bus'
// import router from './router'
//Vue.use(VueRouter);
Vue.use(VueAxios, axios);

Vue.config.productionTip = false;
Vue.prototype.$ebus = new Vue();
//Vue.prototype.$ebus = EventBus;
/* eslint-disable no-new */
new Vue({
  el: '#app',
  
  components: { App },
  template: '<App/>'
})
